#Bootstrap-DateTimePicker (v1.0.0)

## About
This javascript library provies a JQuery plugin that integrates into Bootstrap themes and offers a date(+time) picker via modal dialog.

## Requirements
This plugin requires JQuery 3.+ atleast and Bootstrap v4.+. FontAwesome 5.+ is recommended as by default the plugin used its free svg-icons but those can be changed in the options.

## How To Use
Simply add the sources from the build folder to your template

    <link rel="stylesheet" href="./css/bootstrap-datetimepicker.min.css">
    <script src="./js/bootstrap-datetimepicker.min.js"></script>
    
Make sure that you add the Bootstrap and JQuery files first.
Then add an input element and activate the plugin via JavaScript:

    <div class="form-group row">
        <label for="datepicker1" class="col-2 col-form-label">Date+Time</label>
        <div class="input-group col-10">
            <input type="text"
                    class="form-control datetimepickerValidatePattern"
                    id="datepicker1"
                    maxlength="19"
                    pattern="[0-9]{4}-(0[1-9]|1[012])-(0[1-9]|1[0-9]|2[0-9]|3[01]) (0[0-9]|1[0-9]|2[0-3])(:[0-5][0-9]){2}"
            >
        </div>
    </div>
    <script type="text/javascript">
        $('#datepicker1').DateTimePicker();
    </script>
    
### Options
There are several options to customize the appearence:

    // Icons displayed on the calendar to switch between months
    icons.arrowLeft: 'fas fa-arrow-left'
    icons.arrowRight: 'fas fa-arrow-right'
    
    // Icon to symbolize the calendar
    icons.calendar: 'far fa-calendar-alt'
    
    // Icon to symbolize the clock
    icons.watch: 'far fa-clock'
    
    // If set to true the plugin will let you select a date
    showDate:        true
    
    // If set to true the plugin will let you select a time
    showTime:        true
    
    // Several design classes that are used
    classCenterVertical: 'datetimepickerVCenter'
    classBlink: 'datetimepickerBlink'
    classDisabledCell: 'table-light'
    classSelectedCell: 'btn-primary'
    classDialogButton: 'btn btn-outline-secondary'
    classDialogButtonParent: 'input-group-append'
    
    // Title to display on the modal dialog
    dialogTitle: 'Date/Time Picker'