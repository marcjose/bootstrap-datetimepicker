/*
 =========================================================
 bootstrap-datetimepicker
 version : 1.0.0
 author: Marc Jose <marc@marc-jose.de>
 requires: Bootstrap v4.+, JQuery 3.+, FontAwesome 5.+
 tested with: Bootstrap v4.3
 =========================================================
 */

(function( $ ) {

    $.fn.DateTimePicker = function( options ) {
        // Get element object
        var inputElement = this;

        // Allow user to overwrite defaults
        var customOptions = $.extend( {}, $.fn.DateTimePicker.options, options );
        // Generate random ID in case there is more than just one datetime picker
        customOptions['randomID'] = (function() {
            var alphabet  = 'abcdefghijklmnopqrstuvwxyz';
            var randomID = '';
            for (var i = 16; i > 0; --i) {
                randomID += alphabet[Math.floor(Math.random() * alphabet.length)];
            }
            return randomID;
        }());

        // Set default datetime value if input field contains value already
        if (inputElement.val() != '') {
            var valDate = null;
            if (customOptions.showTime && ! customOptions.showDate) {
                valDate = new Date('1970-01-01 ' + inputElement.val());
            } else {
                valDate = new Date(inputElement.val());
            }
            $.fn.DateTimePicker.dateValue = {
                year: valDate.getFullYear(),
                month: valDate.getMonth(),
                day: valDate.getDate(),
                hour: valDate.getHours(),
                minute: valDate.getMinutes(),
                second: valDate.getSeconds()
            };
        } else {
            $.fn.DateTimePicker.dateValue = {
                year: new Date().getFullYear(),
                month: new Date().getMonth(),
                day: new Date().getDate(),
                hour: new Date().getHours(),
                minute: new Date().getMinutes(),
                second: 0 // Zero seconds as default
            };
        }

        // Append showDialog-button to the input
        appendDialogButton( inputElement, customOptions );

        // Create modal dialog
        createModalDialog( inputElement, customOptions );

        // Populate initial table
        if ( customOptions['showDate'] ) {
            $.fn.DateTimePicker.populateTable(
                customOptions['randomID'],
                new Date(
                    $.fn.DateTimePicker.dateValue['year'],
                    $.fn.DateTimePicker.dateValue['month']
                ),
                customOptions['classDisabledCell'],
                customOptions['classSelectedCell']
            );
        }
        $('#' + customOptions['randomID'] + 'datetimepickerHeader').text(
            $.fn.DateTimePicker.months['_' + $.fn.DateTimePicker.dateValue['month']] + ' - ' + $.fn.DateTimePicker.dateValue['year']
        );

        return this;
    };

    // Date's month value to string
    $.fn.DateTimePicker.months = {
        _0: 'January',
        _1: 'February',
        _2: 'March',
        _3: 'April',
        _4: 'Mai',
        _5: 'June',
        _6: 'July',
        _7: 'August',
        _8: 'September',
        _9: 'October',
        _10: 'November',
        _11: 'December'
    };

    // Default options used during set up
    $.fn.DateTimePicker.options = {
        icons:           {
                             arrowLeft: 'fas fa-arrow-left',
                             arrowRight: 'fas fa-arrow-right',
                             calendar: 'far fa-calendar-alt',
                             watch: 'far fa-clock'
                         },
        showDate:        true,
        showTime:        true,
        classCenterVertical: 'datetimepickerVCenter',
        classBlink: 'datetimepickerBlink',
        classDisabledCell: 'table-light',
        classSelectedCell: 'btn-primary',
        classDialogButton: 'btn btn-outline-secondary',
        classDialogButtonParent: 'input-group-append',
        dialogTitle: 'Date/Time Picker'
    };

    // Fill table with cells for each day of the given month/year combination
    $.fn.DateTimePicker.populateTable = function ( randomId, date, disabledCellClass, selectedCellClass ) {
        var getDay = function(date) {
            var day = date.getDay();
            if (day == 0) {
                day = 7;
            }
            return day - 1;
        };
        var month = date.getMonth();
        var table = document.getElementById(randomId + 'datetimepickerTableBody');
        var rowCount = 0;

        // Clear previous cells
        while (table.firstChild) {
            table.removeChild(table.firstChild);
        }

        // First row
        var currentRow = document.createElement('tr');
        rowCount++;
        table.appendChild(currentRow);
        // Add empty cells if needed at the beginning
        for (i = 0; i < getDay(date); i++) {
            var nonExistingDay = document.createElement('td');
            nonExistingDay.className = disabledCellClass + ' datetimepickerCells ' + randomId;
            currentRow.appendChild(nonExistingDay);
        }

        // Finish rows and add existing days
        while (date.getMonth() == month) {
            var existingDay = document.createElement('td');
            existingDay.innerText = String(date.getDate()).padStart(2, '0');
            existingDay.className = 'datetimepickerCells ' + randomId;
            existingDay.setAttribute('data-value', String(date.getDate()).padStart(2, '0'));
            existingDay.onclick = function() {
                $('#' + randomId + 'Day').val((this.getAttribute('data-value')));

                $("td").each(function() {
                    if ($(this).hasClass(randomId) && $(this).hasClass(selectedCellClass) ) {
                        $(this).removeClass(selectedCellClass);
                    }
                });
                this.className = 'datetimepickerCells ' + randomId + ' ' + selectedCellClass;
            };
            currentRow.append(existingDay);

            if (getDay(date) % 7 == 6) {
                currentRow = document.createElement('tr');
                rowCount++;
                table.appendChild(currentRow);
            }

            date.setDate(date.getDate() + 1);
        }

        // Add empty cells at the end
        if (getDay(date) != 0) {
            for (i = getDay(date); i < 7; i++) {
                var nonExistingDay = document.createElement('td');
                nonExistingDay.className = disabledCellClass + ' datetimepickerCells ' + randomId;
                currentRow.appendChild(nonExistingDay);
            }
        }

        // Make sure that there are always 6 rows to avoid weird resizing
        if (rowCount !== 6 || currentRow.firstChild == null) {
            if (rowCount !== 6 ) {
                currentRow = document.createElement('tr');
                table.appendChild(currentRow);
            }
            for (i = 0; i < 7; i++) {
                var nonExistingDay = document.createElement('td');
                nonExistingDay.className = disabledCellClass + ' datetimepickerCells ' + randomId;
                nonExistingDay.innerHTML = '&nbsp;';
                currentRow.appendChild(nonExistingDay);
            }
        }
    };

    // Populate table with next or previous month' values
    $.fn.DateTimePicker.switchMonth = function( randomId, direction, disabledCellClass, selectedCellClass ) {
        var currentYear = $('#' + randomId + 'Year').val();
        var currentMonth = $('#' + randomId + 'Month').val();

        var nextDate = new Date(currentYear, currentMonth);
        nextDate.setMonth(nextDate.getMonth() + (direction == 'next' ? 1 : -1));

        $('#' + randomId + 'Year').val(nextDate.getFullYear());
        $('#' + randomId + 'Month').val(nextDate.getMonth());
        $('#' + randomId + 'datetimepickerHeader').text(
            $.fn.DateTimePicker.months['_' + nextDate.getMonth()] + ' - ' + nextDate.getFullYear()
        );

        $.fn.DateTimePicker.populateTable( randomId, nextDate, disabledCellClass, selectedCellClass);
    };

    // Append button to the input which opens the modal dialog when clicked
    function appendDialogButton( inputElement, options ) {
        let parentElement = inputElement.parent()[0];

        // Append button to the input field
        let outerElement = document.createElement('div');
        outerElement.className = options.classDialogButtonParent;
        parentElement.appendChild(outerElement);
        // Button label
        let labelElement = document.createElement('i');
        if ( ! options.showDate && options.showTime ) {
            labelElement.className = options.icons.watch;
        } else {
            labelElement.className = options.icons.calendar;
        }
        // Button element
        $.fn.DateTimePicker.showDialogBtn = document.createElement('button');
        $.fn.DateTimePicker.showDialogBtn.type = 'button';
        $.fn.DateTimePicker.showDialogBtn.setAttribute( 'data-toggle', 'modal');
        $.fn.DateTimePicker.showDialogBtn.setAttribute( 'data-target', '#' + options.randomID );
        $.fn.DateTimePicker.showDialogBtn.className = options.classDialogButton;
        $.fn.DateTimePicker.showDialogBtn.appendChild( labelElement );
        outerElement.appendChild( $.fn.DateTimePicker.showDialogBtn );
    }

    // Create modal dialog elements
    function createModalDialog( inputElement, options ) {
        let parentElement = inputElement.parent()[0];

        let dialogOuterElement = document.createElement('div');
        dialogOuterElement.className = 'modal fade ' + ((options.showDate && options.showTime) ? 'bd-example-modal-xl' : '');
        dialogOuterElement.id = options.randomID;
        dialogOuterElement.setAttribute('tabindex', '-1');
        dialogOuterElement.setAttribute('role', 'dialog');
        dialogOuterElement.setAttribute('aria-labelledby', options.dialogTitle );
        dialogOuterElement.setAttribute('aria-hidden', 'true');
        parentElement.appendChild(dialogOuterElement);

        let dialogElement = document.createElement('div');
        dialogElement.className = 'modal-dialog ' + ((options.showDate && options.showTime) ? 'modal-xl' : '') + ' modal-dialog-centered';
        dialogElement.setAttribute('role', 'document');
        dialogOuterElement.appendChild(dialogElement);

        let dialogContent = document.createElement('div');
        dialogContent.className = 'modal-content';
        dialogElement.appendChild(dialogContent);

        let dialogTitle = document.createElement('div');
        dialogTitle.className = 'modal-header  text-center';
        dialogContent.appendChild(dialogTitle);

        let dialogHeader = document.createElement('h5');
        dialogHeader.className = 'modal-title';
        dialogHeader.textContent = 'Pick date and time';
        dialogTitle.appendChild(dialogHeader);

        let dialogClosebutton = document.createElement('button');
        dialogClosebutton.type = 'button';
        dialogClosebutton.className = 'close';
        dialogClosebutton.setAttribute('data-dismiss', 'modal');
        dialogClosebutton.setAttribute('aria-label', 'Close Dialog');
        dialogTitle.appendChild(dialogClosebutton);

        let dialogClosebuttonX = document.createElement('span');
        dialogClosebuttonX.setAttribute('aria-hidden', 'true');
        dialogClosebuttonX.innerHTML = '&times;';
        dialogClosebutton.appendChild(dialogClosebuttonX);

        let dialogBody = document.createElement('div');
        dialogBody.className = 'modal-body';
        dialogContent.appendChild(dialogBody);

        dialogBody.appendChild(createDialogContent( options ));

        let dialogFooter = document.createElement('div');
        dialogFooter.className = 'modal-footer';
        dialogContent.appendChild(dialogFooter);

        let dialogFooterClosebutton = document.createElement('button');
        dialogFooterClosebutton.type = 'button';
        dialogFooterClosebutton.className = 'btn btn-secondary';
        dialogFooterClosebutton.textContent = 'Close';
        dialogFooterClosebutton.setAttribute('data-dismiss', 'modal');
        dialogFooter.appendChild(dialogFooterClosebutton);

        let dialogFooterSubmitbutton = document.createElement('button');
        dialogFooterSubmitbutton.type = 'button';
        dialogFooterSubmitbutton.className = 'btn btn-primary';
        dialogFooterSubmitbutton.textContent = 'Select';
        dialogFooterSubmitbutton.setAttribute('data-dismiss', 'modal');
        dialogFooterSubmitbutton.onclick = function() {
            if ( options.showDate ) {
                var year = $('#' + options.randomID + 'Year').val();
                var month = String(parseInt($('#' + options.randomID + 'Month').val()) + 1).padStart(2, '0');
                var day = $('#' + options.randomID + 'Day').val().padStart(2, '0');
            }
            if ( options.showTime ) {
                var hour = $('#' + options.randomID + 'Hour').val().padStart(2, '0');
                var minute = $('#' + options.randomID+ 'Minute').val().padStart(2, '0');
                var second = $('#' + options.randomID + 'Second').val().padStart(2, '0');
            }
            // yyyy-MM-dd HH:mm:ss
            if (options.showDate && ! options.showTime) {
                inputElement.val(year + '-' + month + '-' + day);
            } else {
                if (! options.showDate && options.showTime) {
                    inputElement.val(hour + ':' + minute + ':' + second);
                } else {
                    inputElement.val(year + '-' + month + '-' + day + ' ' + hour + ':' + minute + ':' + second);
                }
            }
        };
        dialogFooter.appendChild(dialogFooterSubmitbutton);
    }

    // Inner content of the modal dialog which contains the calendar and watch
    function createDialogContent( options ) {
        let containerElement = document.createElement('div');
        containerElement.className = 'container-fluid';

        let rowElement = document.createElement('div');
        rowElement.className = 'row';
        containerElement.appendChild(rowElement);

        if ( options.showDate  === true ) {
            let dateColElement = document.createElement('div');
            dateColElement.id = options.randomID + 'Date';
            dateColElement.className = ((options.showTime) ? 'col-6 ' : 'col-12 ') + options.classCenterVertical;
            rowElement.appendChild(dateColElement);

            let dateContainerElement = document.createElement('div');
            dateContainerElement.className = 'container-fluid';
            dateColElement.appendChild(dateContainerElement);

            let dateRowHeaderElement = document.createElement('div');
            dateRowHeaderElement.className = 'row';
            dateContainerElement.appendChild(dateRowHeaderElement);

            let dateColHeaderElement = document.createElement('div');
            dateColHeaderElement.className = 'col-12';
            dateRowHeaderElement.appendChild(dateColHeaderElement);

            let dateHeaderElement = document.createElement('h1');
            dateHeaderElement.className = 'text-center';
            dateColHeaderElement.appendChild(dateHeaderElement);

            let dateHeaderIconElement = document.createElement('span');
            dateHeaderIconElement.className = options.icons['calendar'];
            dateHeaderElement.appendChild(dateHeaderIconElement);

            let dateRowToolbarContentElement = document.createElement('div');
            dateRowToolbarContentElement.className = 'row';
            dateContainerElement.appendChild(dateRowToolbarContentElement);

            let dateColToolbarPrevContentElement = document.createElement('div');
            dateColToolbarPrevContentElement.className = 'col-md-2 text-center';
            dateColToolbarPrevContentElement.onclick = function() {
                $.fn.DateTimePicker.switchMonth( options.randomID, 'prev', options.classDisabledCell, options.classSelectedCell );
            };
            dateRowToolbarContentElement.appendChild(dateColToolbarPrevContentElement);

            let dateColToolbarPrevArrowElement = document.createElement('span');
            dateColToolbarPrevArrowElement.className = options.icons.arrowLeft;
            dateColToolbarPrevContentElement.appendChild(dateColToolbarPrevArrowElement);

            let dateColToolbarHeaderContentElement = document.createElement('div');
            dateColToolbarHeaderContentElement.className = 'col-md-8 text-center';
            dateRowToolbarContentElement.appendChild(dateColToolbarHeaderContentElement);

            let dateColToolbarHeaderTextElement = document.createElement('strong');
            dateColToolbarHeaderTextElement.id = options.randomID + 'datetimepickerHeader';
            dateColToolbarHeaderContentElement.appendChild(dateColToolbarHeaderTextElement);

            let dateColToolbarNextContentElement = document.createElement('div');
            dateColToolbarNextContentElement.className = 'col-md-2 text-center';
            dateColToolbarNextContentElement.onclick = function() {
                $.fn.DateTimePicker.switchMonth( options.randomID, 'next', options.classDisabledCell, options.classSelectedCell );
            };
            dateRowToolbarContentElement.appendChild(dateColToolbarNextContentElement);

            let dateColToolbarNextArrowElement = document.createElement('span');
            dateColToolbarNextArrowElement.className = options.icons.arrowRight;
            dateColToolbarNextContentElement.appendChild(dateColToolbarNextArrowElement);

            let dateRowTableElement = document.createElement('div');
            dateRowTableElement.className = 'row';
            dateContainerElement.appendChild(dateRowTableElement);

            let dateColTableElement = document.createElement('div');
            dateColTableElement.className = 'col-12';
            dateRowTableElement.appendChild(dateColTableElement);

            let dateInputYear = document.createElement('input');
            dateInputYear.id = options.randomID + 'Year';
            dateInputYear.type = 'number';
            dateInputYear.setAttribute('hidden', true);
            dateColTableElement.appendChild(dateInputYear);
            dateInputYear.value = $.fn.DateTimePicker.dateValue['year'];

            let dateInputMonth = document.createElement('input');
            dateInputMonth.id = options.randomID + 'Month';
            dateInputMonth.type = 'number';
            dateInputMonth.setAttribute('hidden', true);
            dateColTableElement.appendChild(dateInputMonth);
            dateInputMonth.value = $.fn.DateTimePicker.dateValue['month'];

            let dateInputDay = document.createElement('input');
            dateInputDay.id = options.randomID + 'Day';
            dateInputDay.type = 'number';
            dateInputDay.setAttribute('hidden', true);
            dateColTableElement.appendChild(dateInputDay);
            dateInputDay.value = $.fn.DateTimePicker.dateValue['day'];

            let dateTableElement = document.createElement('table');
            dateTableElement.className = 'table table-sm table-bordered text-center';
            dateColTableElement.appendChild(dateTableElement);

            let dateTableHeadElement = document.createElement('thead');
            dateTableElement.appendChild(dateTableHeadElement);

            let dateTableHeadTrElement = document.createElement('tr');
            dateTableHeadTrElement.className = 'table-info';
            dateTableHeadElement.appendChild(dateTableHeadTrElement);

            let dataTableHeadThMonElement = document.createElement('th');
            dataTableHeadThMonElement.innerText = 'Mon';
            dateTableHeadTrElement.appendChild(dataTableHeadThMonElement);

            let dataTableHeadThTueElement = document.createElement('th');
            dataTableHeadThTueElement.innerText = 'Tue';
            dateTableHeadTrElement.appendChild(dataTableHeadThTueElement);

            let dataTableHeadThWedElement = document.createElement('th');
            dataTableHeadThWedElement.innerText = 'Wed';
            dateTableHeadTrElement.appendChild(dataTableHeadThWedElement);

            let dataTableHeadThThuElement = document.createElement('th');
            dataTableHeadThThuElement.innerText = 'Thu';
            dateTableHeadTrElement.appendChild(dataTableHeadThThuElement);

            let dataTableHeadThFriElement = document.createElement('th');
            dataTableHeadThFriElement.innerText = 'Fri';
            dateTableHeadTrElement.appendChild(dataTableHeadThFriElement);

            let dataTableHeadThSatElement = document.createElement('th');
            dataTableHeadThSatElement.innerText = 'Sat';
            dateTableHeadTrElement.appendChild(dataTableHeadThSatElement);

            let dataTableHeadThSunElement = document.createElement('th');
            dataTableHeadThSunElement.innerText = 'Sun';
            dateTableHeadTrElement.appendChild(dataTableHeadThSunElement);

            let dateTableBodyElement = document.createElement('tbody');
            dateTableBodyElement.id = options.randomID + 'datetimepickerTableBody';
            dateTableElement.appendChild(dateTableBodyElement);
        }

        if ( options.showTime  === true ) {
            let timeColElement = document.createElement('div');
            timeColElement.id = options.randomID + 'Date';
            // TODO: If ! showDate
            timeColElement.className = ((options.showDate) ? 'col-md-6 ' : 'col-md-12 ') + 'd-flex align-items-center';
            rowElement.appendChild(timeColElement);

            let timeContainerElement = document.createElement('div');
            timeContainerElement.className = 'container-fluid';
            timeColElement.appendChild(timeContainerElement);

            let timeRowHeaderElement = document.createElement('div');
            timeRowHeaderElement.className = 'row';
            timeContainerElement.appendChild(timeRowHeaderElement);

            let timeColHeaderElement = document.createElement('div');
            timeColHeaderElement.className = 'col-12';
            timeRowHeaderElement.appendChild(timeColHeaderElement);

            let timeHeaderElement = document.createElement('h1');
            timeHeaderElement.className = 'text-center';
            timeColHeaderElement.appendChild(timeHeaderElement);

            let timeHeaderIconElement = document.createElement('span');
            timeHeaderIconElement.className = options.icons['watch'];
            timeHeaderElement.appendChild(timeHeaderIconElement);

            let timeRowContentElement = document.createElement('div');
            timeRowContentElement.className = 'row';
            timeContainerElement.appendChild(timeRowContentElement);

            let timeColContentElement = document.createElement('div');
            timeColContentElement.className = 'col-12';
            timeRowContentElement.appendChild(timeColContentElement);

            let timeInputGroupContentElement = document.createElement('div');
            timeInputGroupContentElement.className = 'input-group';
            timeColContentElement.appendChild(timeInputGroupContentElement);

            let timeInputHourContentElement = document.createElement('input');
            timeInputHourContentElement.id = options.randomID + 'Hour';
            timeInputHourContentElement.type = 'number';
            timeInputHourContentElement.className = 'form-control col-4 text-center';
            timeInputHourContentElement.min = 0;
            timeInputHourContentElement.max = 23;
            timeInputGroupContentElement.appendChild(timeInputHourContentElement);
            timeInputHourContentElement.value = $.fn.DateTimePicker.dateValue['hour'];

            let timeInputAppendContentElement = document.createElement('div');
            timeInputAppendContentElement.className = 'input-group-append';
            timeInputGroupContentElement.appendChild(timeInputAppendContentElement);

            let timeInputAppendSpanElement = document.createElement('span');
            timeInputAppendSpanElement.className = 'input-group-text';
            timeInputAppendContentElement.appendChild(timeInputAppendSpanElement);

            let timeInputAppendStrongElement = document.createElement('strong');
            timeInputAppendStrongElement.className = options.classBlink;
            timeInputAppendStrongElement.innerText = ':';
            timeInputAppendSpanElement.appendChild(timeInputAppendStrongElement);

            let timeInputMinuteContentElement = document.createElement('input');
            timeInputMinuteContentElement.id = options.randomID + 'Minute';
            timeInputMinuteContentElement.type = 'number';
            timeInputMinuteContentElement.className = 'form-control col-4 text-center';
            timeInputMinuteContentElement.min = 0;
            timeInputMinuteContentElement.max = 59;
            timeInputGroupContentElement.appendChild(timeInputMinuteContentElement);
            timeInputMinuteContentElement.value = $.fn.DateTimePicker.dateValue['minute'];

            timeInputAppendContentElement = document.createElement('div');
            timeInputAppendContentElement.className = 'input-group-append';
            timeInputGroupContentElement.appendChild(timeInputAppendContentElement);

            timeInputAppendSpanElement = document.createElement('span');
            timeInputAppendSpanElement.className = 'input-group-text';
            timeInputAppendContentElement.appendChild(timeInputAppendSpanElement);

            timeInputAppendStrongElement = document.createElement('strong');
            timeInputAppendStrongElement.className = options.classBlink;
            timeInputAppendStrongElement.innerText = ':';
            timeInputAppendSpanElement.appendChild(timeInputAppendStrongElement);

            let timeInputSecondContentElement = document.createElement('input');
            timeInputSecondContentElement.type = 'number';
            timeInputSecondContentElement.className = 'form-control col-4 text-center';
            timeInputSecondContentElement.min = 0;
            timeInputSecondContentElement.max = 59;
            timeInputGroupContentElement.appendChild(timeInputSecondContentElement);
            timeInputSecondContentElement.id = options.randomID + 'Second';
            timeInputSecondContentElement.value = $.fn.DateTimePicker.dateValue['second'];
        }
        return containerElement;
    }

}( jQuery ));